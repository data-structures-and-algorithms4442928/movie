
package com.mycompany.group;



public class Cinema {
    private static String[][] stringTable = new String[4][6];
    private static String[] x = { "A", "B", "C", "D" };
    private static String[] y = { "1", "2", "3", "4", "5", "6" };
    private static int collect = 0;
    private static int pre = 240;
    private static int normal = 130;

    public static void createTable(String[] x, String[] y) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < y.length; j++) {
                stringTable[i][j] = x[i] + y[j];
            }
        }
    }
    
    public int getCollect() {
       return collect;
    }

    public static void selectChair(String num) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < y.length; j++) {
                if (num.equals(stringTable[i][j])) {
                    stringTable[i][j] = "X";
                    if(i >= 2){
                        collect =  collect + pre;
                    }else{
                        collect =  collect + normal;
                    }
                } //else if (stringTable[i][j].equals("X")) {
                   // System.out.println("this seat not available");
               // }
            }
        }
    }

    public static void printTable() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {

                System.out.print(stringTable[i][j] + " ");
            }
            System.out.println();
        }
    }
}

