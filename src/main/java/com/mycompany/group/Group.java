
package com.mycompany.group;

import java.util.Scanner;

public class Group {
    public static void main(String[] args) {
        int aa = 0;
        
        Scanner sn = new Scanner(System.in);
        Cinema cinema = new Cinema();
        Booking booking = new Booking();
        
        booking.welcome();
        booking.Book();
        
        String[] x = { "A", "B", "C", "D" };
        String[] y = { "1", "2", "3", "4", "5", "6" };
        cinema.createTable(x, y);
        
        String seat[] = new String[1000000];
        
        System.out.println("--- ที่นั่งที่รองรับ ---");
        System.out.println(">>> ที่นั่งแถว A - B เป็นที่นั่งธรรมดาราคา 130 บาท");
        System.out.println(">>> ที่นั่งแถว C - D เป็นที่นั่งธรรมดาราคา 240 บาท");
        
        outerLoop: while(true){
            cinema.printTable();
            System.out.println(">> กรุณาเลือกตำแน่งที่นั่งที่ต้องการ : ");
            String input_chair = sn.next();
            boolean check_error = false;
            
            for(int b = 0;b < aa;b++){
                if (seat[b].equals(input_chair)){
                    System.out.println(">> ที่นั่งที่ท่านเลือกไม่ว่างในขณะนี้");
                    System.out.println(">> ต้องการที่จะเลือกต่อไหม ใช่ = 1 / ไม่ = 2");
                    int ff = sn.nextInt();
                    if(ff == 1){
                        check_error = true;
                    }else if(ff == 2){
                        break outerLoop;
                    }
                }
            }
            
            if(check_error == true){
                continue;
            }
            
            seat[aa] = input_chair;
            cinema.selectChair(seat[aa]);
            cinema.printTable();
            System.out.println(">> ต้องการที่จะเลือกต่อไหม ใช่ = 1 / ไม่ = 2");
            int ff = sn.nextInt();
            aa++;
            if(ff == 1){
            }else if(ff == 2){
               break;
            }
       }   
        
        String seat_picked[] = new String[aa];
        
        booking.result();
        System.out.print(">> ตำแหน่งที่ท่านเลือก :");
        for(int b = 0;b <= seat_picked.length - 1;b++){
            seat_picked[b] = seat[b];
            System.out.print(" "+seat_picked[b]);
        }
        System.out.println("");
        System.out.print(">> ราคารวมทั้งหมดที่ต้องจ่าย : "+cinema.getCollect() + " บาท");
        
    }
}

