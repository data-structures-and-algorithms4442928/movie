
package com.mycompany.group;

import java.util.Scanner;


public class Booking {
    
        String movie_name[] = new String[3];
        String M1 = "The Meng Story";
        String M2 = "New Galaxy";
        String M3 = "Cosmos Master";
        
        String movie_sub[] = new String[2];
        String PakTH = "พากย์ไทย";
        String PakENG = "ซับไทย";
        
        String movie_time[] = new String[3];
        String MTime1 = "12:00";
        String MTime2 = "15:00";
        String MTime3 = "18:00";
        
        int movie_picked;
        int sub_picked;
        int time_picked;
        
        String movie_pickedf;
        String sub_pickedf;
        String time_pickedf;
        
        
    
    public void Book(){
        Scanner sn = new Scanner(System.in);
        
        movie_name[0] = M1;
        movie_name[1] = M2;
        movie_name[2] = M3;
        
        movie_sub[0] = PakTH;
        movie_sub[1] = PakENG;
        
        movie_time[0] = MTime1;
        movie_time[1] = MTime2;
        movie_time[2] = MTime3;
        
        System.out.println("--- รายชื่อหนังที่เข้าฉายในขณะนี้ ---");
        
        for(int a = 0;a <= movie_name.length - 1;a++ ){
            System.out.println((a+1)+"."+movie_name[a]);
        } 
        
        System.out.println(">> กรุณาพิมพ์หมายเลขหนังที่ต้องการ : ");
        
        movie_picked = sn.nextInt();
        movie_pickedf = "";
        
        System.out.println("--- เสียงพากย์ที่รองรับ ---");
        
        for(int a = 0;a <= movie_sub.length - 1;a++ ){
            System.out.println((a+1)+"."+movie_sub[a]);
        } 
        
        System.out.println(">> กรุณาเลือกเสียงพากย์ที่ต้องการ : ");
        
        sub_picked = sn.nextInt();
        sub_pickedf = "";
        
        System.out.println("--- เวลาที่รองรับ ---");
        
        for(int a = 0;a <= movie_time.length - 1;a++ ){
            System.out.println((a+1)+"."+movie_time[a]);
        } 
        
        System.out.println(">> กรุณาเลือกเวลาที่ต้องการ : ");
        
        time_picked = sn.nextInt();
        time_pickedf = "";
        
        if(movie_picked == 1){
            movie_pickedf = M1;
        }else if(movie_picked == 2){
            movie_pickedf = M2;
        }else if(movie_picked == 3){
            movie_pickedf = M3;
        }
        
        if(time_picked == 1){
            time_pickedf = MTime1;
        }else if(time_picked == 2){
            time_pickedf = MTime2;
        }else if(movie_picked == 3){
            time_pickedf = MTime3;
        }
        
        if(sub_picked == 1){
            sub_pickedf = PakTH;
        }else if(sub_picked == 2){
            sub_pickedf = PakENG;
        }
    }
    
    public void result(){
        System.out.println("--- รายละเอียดรายการของท่าน ---");
        System.out.println(">> หนังที่ท่านเลือก : "+movie_pickedf);
        System.out.println(">> เสียงพากย์ที่ท่านเลือก : "+sub_pickedf);
        System.out.println(">> เวลาที่ท่านเลือก : "+time_pickedf);
    }
    
    public void welcome(){
        System.out.println("--- Welcome To XXX Cinema ---");
        System.out.println("");
        System.out.println(">> ทางเราจะนำท่านไปสู่ระบบจองที่นั่ง (* - *)ง");
        System.out.println("");
    }
}
